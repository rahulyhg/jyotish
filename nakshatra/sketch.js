/*
 * @name Pie Chart
 * @description Uses the arc() function to generate a pie chart from the data 
 * stored in an array.
 */

var angles = new Array();
var angles27 = new Array();
var FIRST_R = 120;

var naks = ["Ashwini", "Bharani", "Krittika", "Rohini", "Mrigashīra", "Ardra", "Punarvasu", "Pushya", "Ashleshā", "Maghā", "Pūrva Phalgunī", "Uttara Phalgunī", "Hasta", "Chitra", "Swāti", "Visakha", "Anuradha", "Jyeshtha", "Mula", "Purva Ashadha", "Uttara Ashadha", "Shravana", "Dhanishta", "Shatabhisha", "Purva Bhdrapada", "Uttara Bhadrapada", "Revati"];
var zodiacs = ["牡羊", "牡牛", "双子", "蟹", "獅子", "乙女", "天秤", "蠍", "射手", "山羊", "水瓶", "魚"];
var lords = ["☋","♀","☉","☽","♂","☊","♃","♄","☿"];
var nakWords = {};

var nakArray = new Array();

function setup() {
  createCanvas(1200, 900);
  // stroke(0.5);
  noLoop(); // Run once and stop
  textFont("Georgia");
  textSize(15);
  fill(50);

  getCSVFile();

  for (var j = 0; j < 12; j++) {
    angles.push(j * 30);
  }
  for (var k = 0; k < 27; k++) {
    angles27.push(k * 13.3333);
  }
}

function getCSVFile() {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
    createArray(xhr.responseText);
    };
 
    xhr.open("get", "nakshatrasJP.csv", true);
    xhr.send(null);
}

 
function createXMLHttpRequest() {
    var XMLhttpObject = null;
    XMLhttpObject = new XMLHttpRequest();
    return XMLhttpObject;
}
 
function createArray(csvData) {
    var tempArray = csvData.split("\n");
    
    for(var i = 0; i<tempArray.length;i++){
    nakArray[i] = tempArray[i].split(",");
    }
    console.log(nakArray);
}


function draw() {
  // background(100);
  drawCircle12(950, angles);
  drawCircle27(750, angles27);
  // textAlign(CENTER);
  fill(0,0,0);
  writeNames();
}

function writeNames() {
  translate(600, 450);
  textAlign(CENTER, CENTER);
  for (var z = 0; z < 12; z++) {
    text(zodiacs[z], cos(radians(z * 30 - FIRST_R +15)) * 440, sin(radians(z * 30 - FIRST_R +15)) * 440); //星座名表示
  }


  for (var r = 0; r < 27; r++) {
    // push();
    text(r + 1, cos(radians(r * 13.333 - FIRST_R + 13.333/2)) * 252, sin(radians(r * 13.333 - FIRST_R + 13.333/2)) * 252); //ナクシャトラナンバー表示
    text(lords[r%9], cos(radians(r * 13.333 - FIRST_R + 13.333/2)) * 235, sin(radians(r * 13.333 - FIRST_R + 13.333/2)) * 235); //ナクシャトラロード表示
    text(naks[r], cos(radians(r * 13.333  - FIRST_R + 13.333/2)) * 320, sin(radians(r * 13.333  - FIRST_R + 13.333/2)) * 320); //ナクシャトラ名表示

    var num = Math.floor((r * 13.333 - Math.floor(r * 13.333)) * 10); //少数第1位が9かどうかチェック。
    var min = r % 3;
    if (num != 9) { //限りなく.9999に近い場合の切り上げ
      var nakR = Math.floor((r * 13.33)) % 30;
    } else {
      var nakR = Math.round((r * 13.33)) % 30;
    }

    var str = '';
    switch (min) { //分の表示
      case 0:
        str = "°00'";
        break;
      case 1:
        str = "°20'";
        break;
      case 2:
        str = "°40'";
        break;
      default:
        str = '';
        break;
    }
    stroke(0.5);
    text(nakR + str, cos(radians(r * 13.333 - FIRST_R)) * 400, sin(radians(r * 13.333 - FIRST_R)) * 400); //度分表示
    noStroke();
  }
}

function drawCircle27(diameter, data) {
  var lastAngle27 = -radians(FIRST_R);
  for (var t = 0; t < data.length; t++) {
    var gray = color(255 * (1 - angles27[t % 9] / 360), 255 * (1 - angles27[t % 9] / 360), 255 * (1 - angles27[t % 9] / 360));
    fill(gray);
    // fill(255,255,255);
    // noFill();
    arc(width / 2, height / 2, diameter, diameter, lastAngle27, lastAngle27 + radians(13.3333), PIE);
    lastAngle27 += radians(13.3333);
    // console.log(lastAngle27);
  }
  fill(255,255,255);
  ellipse(width/2,height/2,450);

}

function drawCircle12(diameter, data) {

  var lastAngle = -radians(FIRST_R);
  for (var i = 0; i < data.length; i++) {
    var gray = color(255 * (1 - angles[i] / 360), 255 * (1 - angles[i] / 360), 255 * (1 - angles[i] / 360));
    var c = i % 4;
    var col = color(255, 102, 102);
    switch (c) {
      case 0:
        col = color(255, 102, 102);
        break; //fire
      case 1:
        col = color(0, 153, 0);
        break; //earth
      case 2:
        col = color(255, 255, 204);
        break; //wind
      case 3:
        col = color(102, 178, 255);
        break; //water
    }
    fill(col);
    // fill(255,255,255);
    // noFill();
    arc(width / 2, height / 2, diameter - 100, diameter - 100, lastAngle, lastAngle + radians(30), PIE);
    lastAngle += radians(30);

    // console.log(lastAngle);
  }
}

var showX = 0;
var showY = 0;
var showAtan = "";
var foreNak=0;
var txt = [];
var count = 0;

function touchEnded(){
  mouseMoved();
}

function mouseMoved(){

  showX = mouseX - 600;
  showY = mouseY - 450;
  showAtan = Math.atan2(showY, showX) * 180 / PI + FIRST_R;//arc tangentでラジアン角を求め、角度へ変換。
  if (showAtan < 0) {
    showAtan = showAtan + 360;
  }
  var nakNum = Math.ceil(showAtan / 13.333);
  if(foreNak != nakNum){
    // text("X:"+showX,-500,-350);
    // text("Y:"+showY,-500,-325);
    fill(255, 255, 255);
    noStroke();
    ellipse(0, 0, 448);
    fill(0, 0, 0);
    stroke(1);textSize(20);
    textAlign(CENTER,TOP);
    text(nakNum + ":" + naks[nakNum-1], 0, -180);//ナクシャトラ名の表示
    noStroke();textSize(15);
    text(nakArray[nakNum][1], 0, -198);//ナクシャトラ名日本語
    text(nakArray[nakNum][2], 0, -145);//ナクシャトラキーワード
    text(nakArray[nakNum][3], 0, -120);//ナクシャトラ名の表示
    if(count>0){txt[count-1].remove();}
    txt[count] = createDiv(nakArray[nakNum][5]);
    txt[count].position(425, 400);
    txt[count].class('nakExp');
    
    foreNak = nakNum;
    count++;
  }
}
