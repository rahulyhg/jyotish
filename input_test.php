<!DOCTYPE html>
<html>
<head>
  <script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-app.js"></script>
  <script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-auth.js"></script>
  <script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-database.js"></script>
  <script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-messaging.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="style.css">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.js" integrity="sha256-tA8y0XqiwnpwmOIl3SGAcFl2RvxHjA8qp0+1uCGmRmg=" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANgyp2Vza8d1KdOPR1rpzhW_RW9JCWESg&callback=initMap"></script>

    <script type="text/javascript">
    //<![CDATA[

    var map;
    var geo;
    
    function gmap() {
      var latlng = new google.maps.LatLng(37, 138);
      var opts = {
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: latlng
      };
      map = new google.maps.Map(document.getElementById("map"), opts);
      geo = new google.maps.Geocoder();
    }

    function buttonpress() {
      // GeocoderRequest
      var req = {
        address: document.getElementById("input").value,
      };
      geo.geocode(req, geoResultCallback);
    }

    function geoResultCallback(result, status) {
      if (status != google.maps.GeocoderStatus.OK) {
        alert(status);
        return;
      }
      var latlng = result[0].geometry.location;
      var lat = result[0].geometry.location.lat();
      var lng = result[0].geometry.location.lng();
      
      map.setCenter(latlng);

      var marker = new google.maps.Marker({position:latlng, map:map, title:latlng.toString(), draggable:true});

      google.maps.event.addListener(marker, 'dragend', function(event){
        marker.setTitle(event.latLng.toString());
      });

      document.getElementById("lattext").value = lat.toString();
      document.getElementById("lngtext").value = lng.toString();
    }

    $(function(){
      $.ajax({
        // リクエストメソッド(GET,POST,PUT,DELETEなど)
        type: 'GET',
        // リクエストURL
        url: 'https://maps.googleapis.com/maps/api/timezone/json',
        // タイムアウト(ミリ秒)
        timeout: 10000,
        // キャッシュするかどうか
        cache: true,
        // サーバに送信するデータ(name: value)
        data: {
          'location': '39.6034810,-119.6822510',
          'timestamp': '1331161200',
          'key': 'AIzaSyANgyp2Vza8d1KdOPR1rpzhW_RW9JCWESg'
        },
        // レスポンスを受け取る際のMIMEタイプ(html,json,jsonp,text,xml,script)
        // レスポンスが適切なContentTypeを返していれば自動判別します。
        dataType: 'json',
      }).done(function(response){
        console.log(response);
        if(response.timeZoneId != null){
        //do something
        alert(response.timeZoneId);
        }
      });
    });
    //]]>
    </script>
  <title>生年月日入力</title>

    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0px; padding: 0px }
      #map { height: 500px }
    </style>

</head>
  <div class="container">
  
  <div class="col-lg-6 col-md-6 col-sm-12">
    <h2>Jyotish Chart 出生情報入力フォーム</h2>

    <div id="birth-form">
      <form action="http://jyotish.terraschole.com/index.php" method="get">

      <h3>■お名前を入力してください（ローマ字）</h3>
      姓<input type="text" name="family_name">
      名<input type="text" name="first_name">

      <h3>■生年月日を入力してください</h3>
      西暦<input type="text" name="year" size=10>年
      <input type="text" name="month" size=5>月
      <input type="text" name="day" size=5>日
      <br />

      <h3>■誕生時刻を入力してください</h3>
      <input type="text" name="hour" size=5>時
      <input type="text" name="min" size=5>分
      　　(時間は「14」時などの<span style="color:red;">24時間表記</span>で！)
      <br />

      <h3>■生誕地を入力して「緯度経度取得」を押してください</h3>
      <div><input id="input" name="birth_place" onsubmit="buttonpress()" /><input type="button" onclick="buttonpress()" value="緯度経度取得" /></div><br />
      <p>（出力された位置が正しく示されているか、下の地図上で念のためご確認ください）</p>
      （自動出力：緯度<input type="text" name="lat" id="lattext">
      経度<input type="text" name="lng" id="lngtext">）
      <br /><br />
      <h3>■入力し終わったら以下の出力ボタンを押してください</h3>
      <p>何も入力していない場合は、現在時刻のチャートが出力されます。</p>
      <button type="submit" name="submit" value="send" style="width:300px;height:80px;">上記情報でチャートを表示<br />(未入力の場合は現在時刻のチャート)</button><br /><br />
        
      </form>
    </div><!-- birth-form -->
    <div id="map"></div>
  </div><!-- col- -->

  <div class="col-lg-6 col-md-6 col-sm-12">
    <h2>Jyotish登録情報データベース</h2>
  </div>  

</body>
<script src="https://www.gstatic.com/firebasejs/4.3.1/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAN8amT7T4wMvZH37-TV_fVqGPxI6YD2t4",
    authDomain: "jyotish-one.firebaseapp.com",
    databaseURL: "https://jyotish-one.firebaseio.com",
    projectId: "jyotish-one",
    storageBucket: "jyotish-one.appspot.com",
    messagingSenderId: "99716757920"
  };
  firebase.initializeApp(config);
</script>
<script src="database.js"></script>

</html>
