<!DOCTYPE html>
<html>
<head>
  <title>Jyotish Charts</title>
  <script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-app.js"></script>
  <script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-auth.js"></script>
  <script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-database.js"></script>
  <script src="https://www.gstatic.com/firebasejs/4.1.1/firebase-messaging.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="style.css">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.js" integrity="sha256-tA8y0XqiwnpwmOIl3SGAcFl2RvxHjA8qp0+1uCGmRmg=" crossorigin="anonymous"></script>
</head>
<body id="charts">
  <div class="container">
    <a href="./input.php" >出生情報入力ページへ</a><br />

<?php 
// エラー出力
ini_set( 'display_errors', 1 );

// path to swiss ephemeris library files
$libPath = './sweph/';
putenv("PATH=$libPath");

echo "現在時刻：".date('Y年m月d日 H時i分s秒')."<br>";
$yearNow = date("Y");
$monthNow = date("m");
$dayNow = date("d");
$hourNow = date("H");
$minNow = date("i");

//出生情報
$family_name =  (!(isset($_GET["family_name"]))||$_GET["family_name"]=="") ? "Test" : mb_convert_kana(htmlspecialchars($_GET["family_name"]),"a", 'UTF-8') ;
$first_name =  (!(isset($_GET["first_name"])) ||$_GET["first_name"]=="")? "Now" : mb_convert_kana(htmlspecialchars($_GET["first_name"]),"a", 'UTF-8') ;
$year =  (!(isset($_GET["year"]))||$_GET["year"]=="")? $yearNow:mb_convert_kana(htmlspecialchars($_GET["year"]),"a", 'UTF-8') ;
$month =  (!(isset($_GET["month"]))||$_GET["month"]=="")? $monthNow:mb_convert_kana(htmlspecialchars($_GET["month"]),"a", 'UTF-8') ;
$day =  (!(isset($_GET["day"]))||$_GET["day"]=="")?$dayNow:mb_convert_kana(htmlspecialchars($_GET["day"]),"a", 'UTF-8') ;
$hour =  (!(isset($_GET["hour"]))||$_GET["hour"]=="")?$hourNow:mb_convert_kana(htmlspecialchars($_GET["hour"]),"a", 'UTF-8') ;
$min = (!(isset($_GET["min"]))||$_GET["min"]=="")?$minNow:mb_convert_kana(htmlspecialchars($_GET["min"]),"a", 'UTF-8') ;
$birthDate = new DateTime( $year."-".$month."-".$day."T".$hour.":".$min.":00");
// Latitude Longitude
$lat = (!(isset($_GET["lat"]))||$_GET["lat"]=="")?35.689:mb_convert_kana(htmlspecialchars($_GET["lat"]),"n", 'UTF-8') ;
$lng = (!(isset($_GET["lng"]))||$_GET["lng"]=="")?139.691:mb_convert_kana(htmlspecialchars($_GET["lng"]),"n", 'UTF-8') ;
$zone =  (!(isset($_GET["zone"]))||$_GET["zone"]=="")?9:mb_convert_kana(htmlspecialchars($_GET["zone"]),"a", 'UTF-8') ;
$birth_place = (!(isset($_GET["birth_place"]))||$_GET["birth_place"]=="")?"Tokyo":htmlspecialchars($_GET["birth_place"]) ;

//Timezone
$timezone = 9; 
if($zone!=9){
  
}

// Converting birth date/time to UTC
$offset = $timezone * (60 * 60);
$birthTimestamp = strtotime($birthDate->format('Y-m-d H:i:s'));
$utcBirthTimestamp = $birthTimestamp - $offset;

$date = date('d.m.Y', $utcBirthTimestamp);
$time = date('H:i:s', $utcBirthTimestamp);
$h_sys = 'A';
// 角度、速度を格納
exec ("swetest -edir$libPath -b$date -ut$time -p0123456789DAmt -sid1 -eswe -house$lng,$lat,$h_sys -flsP -g, -head", $planets);

//各惑星情報を変数に格納
$asc = explode(",", $planets[26]);//0
$su = explode(",", $planets[0]);//1
$mo = explode(",", $planets[1]);//2
$me = explode(",", $planets[2]);//3
$ve = explode(",", $planets[3]);//4
$ma = explode(",", $planets[4]);//5
$ju = explode(",", $planets[5]);//6
$sa = explode(",", $planets[6]);//7
$ra = explode(",", $planets[13]);//8
$keD = $ra[0]<180 ? $ra[0] + 180 : $ra[0] - 180 ;//9

//各種情報の配列代入
$graha = array("Asc","Su","Mo","Me","Ve","Ma","Ju","Sa","Ra","Ke");
$deg = array($asc[0],$su[0],$mo[0],$me[0],$ve[0],$ma[0],$ju[0],$sa[0],$ra[0],$keD);//360度
$speed = array(0,$su[1],$mo[1],$me[1],$ve[1],$ma[1],$ju[1],$sa[1],$ra[1],$ra[1]);//1日の速度（度数）
//一般的インド式情報格納
$karaka_name = array("AK","AmK","Bk","MK","PK","GK","DK");
$rasi = array("Ari","Tau","Gem","Can","Leo","Vir","Lib","Sco","Sag","Cap","Aqu","Pis");
$rasiJP = array("牡羊","牡牛","双子","蟹","獅子","乙女","天秤","蠍","射手","山羊","水瓶","魚");
$naks = array("Ashwini", "Bharani", "Krittika", "Rohini", "Mrigashīra", "Ardra", "Punarvasu", "Pushya", "Ashleshā", "Maghā", "Pūrva Phalgunī", "Uttara Phalgunī", "Hasta", "Chitra", "Swāti", "Visakha", "Anuradha", "Jyeshtha", "Mula", "Purva Ashadha", "Uttara Ashadha", "Shravana", "Dhanishta", "Shatabhisha", "Purva Bhdrapada", "Uttara Bhadrapada", "Revati");
$vim_icons = array("☋","♀","☉","☽","♂","☊","♃","♄","☿");
$vim_graha = array("Ke","Ve","Su","Mo","Ma","Ra","Ju","Sa","Me");
//ムリチュバーギャ角度（星座順通り）
$mb["Asc"]=array(1,9,22,22,25,2,4,23,18,20,24,10);
$mb["Su"]=array(20,9,12,6,8,24,16,17,22,2,3,23);
$mb["Mo"]=array(26,12,13,25,24,11,26,14,13,25,5,12);
$mb["Me"]=array(15,14,13,12,8,18,20,10,21,22,7,5);
$mb["Ve"]=array(28,15,11,17,10,13,4,6,27,12,29,19);
$mb["Ma"]=array(19,28,25,23,29,28,14,21,2,15,11,6);
$mb["Ju"]=array(19,29,12,27,6,4,13,10,17,11,15,28);
$mb["Sa"]=array(10,4,7,9,12,16,3,18,28,14,13,15);
$mb["Ra"]=array(14,13,12,11,24,23,22,21,10,20,18,8);
$mb["Ke"]=array(8,18,20,10,21,22,23,24,11,12,13,14);

//各種配列準備
$rasiNum = array();//星座ナンバー
$houseNum = array();//ハウスナンバー
$reverse = array();//逆行ありなし
$degInside = array();//ハウス内の角度
$degInsideMin = array();//ハウス内の角度の度分表示
$element4 = array();//4属性
$degNava = array();//ナヴァムシャにおける角度（【Do】未計算）
$rasiNumNava = array();//ナヴァムシャにおける星座番号
$houseNumNava = array();//ナヴァムシャにおけるハウスナンバー
$nava9 = array();//ラーシチャートからナヴァムシャに変換するための9区画
$nakNum = array();//ナクシャトラの番号
$nakName = array();//ナクシャトラ名
$nakGrahaNum = array();//ナクシャトラの支配星番号
$nakGrahaName = array();//ナクシャトラの支配星名

//deg,speed を四捨五入
function round2(&$item,$key){$item = round($item,2);}
array_walk($deg, "round2");//角度を小数第2位に丸める
array_walk($speed, "round2");//速度を丸める

//rasi & house number　＆ハウス内の角度などを計算。アセンダントと、9惑星
for($j=0;$j<10;$j++){
  $rasiNum[$j] = floor($deg[$j] /30) ;//星座ナンバー取得 0~11
  $nakNum[$j] = floor($deg[$j]/360*27);//ナクシャトラの順番を取得0~26
  $nakName[$j] = $naks[$nakNum[$j]];//ナクシャトラ名
  $nakGrahaNum[$j] = $nakNum[$j]%9;//ナクシャトラの支配惑星ナンバーを求める
  $nakGrahaName[$j] = $vim_graha[$nakGrahaNum[$j]];//ナクシャトラの支配惑星名
  $houseNum[$j] = $rasiNum[$j] - $rasiNum[0] + 1;//ハウスナンバー取得
  if($houseNum[$j]<=0){$houseNum[$j] = $houseNum[$j] + 12;}//1~12
  if($speed[$j]<0&&$j<8){$reverse[$j]="R";}
  elseif($speed[$j]>0&&$j>7){$reverse[$j]="R";}
  else{$reverse[$j]="";}//逆行取得

  $degInside[$j] = round(($deg[$j] - $rasiNum[$j]*30)*100)/100;//ハウス内の角度取得。整数部分が0の時の小数計算がおかしいので、roundと100倍と100で割る作業をする。php。。。
}

//ハウス内の角度を度分表示に
$degInsideMin = deg2min($degInside);
$speed2min = deg2min($speed);
function deg2min($arr){
  $degMin = array();
  foreach ($arr as $key => $item){
    $number[0] = "";$number[1]= "";
    $pos = strpos($item,".");
    if($pos){
      $number = explode('.', strval($item));
      $degHere = $number[0];
      $minHere = (!$number[1])?0:round($number[1]/100*60);
    }else{
      $degHere = $item;
      $minHere = 0;
    }
    $minHere = ($minHere<10)?"0".$minHere:$minHere;
    $full = $degHere.":".$minHere;
    $degMin[$key] = $full;
  }
  return $degMin;
}

//ダシャーの算出
  /*　get_maha_dasha() 
  param $nak_num ナクシャトラの番号0~26
  param $nak_graha_num ジャンマナクシャトラの支配星番号
  param $nakName ジャンマナクシャトラ名
  param $utc 生まれ時間のutcタイムスタンプ
  param $vim_graha ナクシャトラの支配星配列
  */
  function get_maha_dasha($mo_deg, $nak_num, $nak_graha_num,$nakName,$utc){    
    //最初のマハーダシャー計算
    $vim_graha = array("Ke","Ve","Su","Mo","Ma","Ra","Ju","Sa","Me");
    $ruler = $vim_graha[$nak_graha_num];
    $maha_years = array("7","20","6","10","7","18","16","19","17");//マハーダシャー年数定義
    $entering_deg = 360/27*$nak_num;//在住ナクシャトラのスタート地点の角度
    $gap_deg = $mo_deg - $entering_deg;//ナクシャトラ内でどこまで進んでいるかの角度算出
    $starting_ratio = $gap_deg/(360/27);//ナクシャトラ内での進み具合のパーセンテージ
    $start_year = $starting_ratio*$maha_years[$nak_graha_num];//最初のマハーダシャースタート地点から出生時までの経過年
    $start_day = $start_year*365;//一年365日として$start_yearを日数に変換
    $remaining_year = $maha_years[$nak_graha_num] - $start_year;//誕生日から次のマハーダシャーまでの年数計算
    $remaining_day = $maha_years[$nak_graha_num]*365 - $start_day;//誕生日から次のマハーダシャーまでの日数計算

    //ダシャー情報表示
    $dasha_str = "ジャンマナクシャトラ：%s(%d),支配星：%s, ナクシャトラ開始角度:%s, ナクシャトラ内での角度:%s<br> 最初のマハーダシャー内経過年:%s(経過日:%s), 2番目のマハーダシャーまでの必要年:%s(必要日:%s)<br />";
    echo sprintf($dasha_str,$nakName,($nak_num+1),$ruler,round($entering_deg,2), round($gap_deg,2), round($start_year,2),round($start_day,2),round($remaining_year,2), round($remaining_day,2));

    //次のマハーダシャー計算
    $maha_dasha_timestamps = array();//各マハーダシャーのタイムスタンプ格納用配列
    $next_dasha_timestamp="";//タイムスタンプ格納用変数
    for($i=0;$i<9;$i++){
      $next_dasha_day = "";//各ダシャーの日付の格納用変数
      $dasha_num_now = ($nak_graha_num+$i>8)?$nak_graha_num+$i-9:$nak_graha_num+$i;//各ダシャー番号

      if($i==0){
        $next_dasha_timestamp = $remaining_day*24*60*60 + $utc;//2番目のダシャーのタイムスタンプ
        $first_dasha_timestamp = $next_dasha_timestamp - round($maha_years[$nak_graha_num]*365.2425*24*3600);//2番目のマハーダシャーの起点から最初のマハーダシャー年数を引いてタイムスタンプ取得
        $first_dasha_day = get_date($first_dasha_timestamp);//最初のダシャーの日付出力
        echo sprintf("%s番目のマハーダシャー%s期(%s)　開始日:%s<br />",1,$vim_graha[$nak_graha_num],$nak_graha_num+1 ,$first_dasha_day); //マハーダシャー情報の出力
        $maha_dasha_timestamps[]=round($first_dasha_timestamp);
        $maha_dasha_timestamps[]=round($next_dasha_timestamp);
      }else{
        $next_dasha_timestamp = $next_dasha_timestamp + round($maha_years[$dasha_num_now]*365.2425*24*3600);//3番目以降のダシャーのタイムスタンプ
        $maha_dasha_timestamps[]=round($next_dasha_timestamp);
      }
      $next_dasha_day = date('Y年m月d日', $next_dasha_timestamp);//各ダシャーの日付の出力

      $next_dasha_num = ($nak_graha_num+$i>7)?$nak_graha_num+$i-8:$nak_graha_num+$i+1;//次のダシャー番号
      echo sprintf("%s番目のマハーダシャー%s期(%s)　開始日:%s<br />",$i+2,$vim_graha[$next_dasha_num],$next_dasha_num +1,$next_dasha_day);//マハーダシャー情報の出力
      
    }
    return $maha_dasha_timestamps;//10個のtimestampを格納
  }

  function get_date($timestamp){
    return date('Y年m月d日',$timestamp);
  }

  //アンタルダシャーの実装
  /*
  運用的には、配列を多重化しないほうが楽！単純にプラティアンタルダシャーのtimestampを全部同じ第一階層に突っ込む。keyに3段階の惑星を突っ込む。6文字でVeJuMoみたいにkeyにする。それを2文字ずつ抜き出して使う！
  だから、3段階のforループでつくりあげる。
  */
  function get_anthar_dasha($maha_dasha_timestamps,$nak_graha_num){
    $vim_graha = array("Ke","Ve","Su","Mo","Ma","Ra","Ju","Sa","Me");
    $start_graha = $vim_graha[$nak_graha_num];//始まりの惑星
    $dasha_ratio = array("7","20","6","10","7","18","16","19","17");//ダシャー比率定義定義
    $maha_timestamps_D120  = array();//各マハーダシャー期間のtimestampを120分割した数値格納配列
    $anthar_dasha_timestamps = array();//各9ダシャー期間の中の細分化されたダシャーの開始日をタイムスタンプで表示。最終成果物。
    $start_graha_num = array_search($start_graha, $vim_graha);//始まりの惑星番号を取得

    $count = count($maha_dasha_timestamps) - 1;//マハーダシャーの配列数を数えて、一つ引いておく
    //10のmaha_dasha_timestampsから差分を取って行って120で割り、9つ格納する。
    for($i=0;$i<$count;$i++){
      $maha_timestamps_D120[] = $maha_dasha_timestamps[$i+1] - $maha_dasha_timestamps[$i];
    }
    //多重配列$dasha9にアンタルダシャーを格納していく。まず、トップレベルにマハーダシャー。セカンドレベルにアンタルダシャー。
    for($j=0;$j<$count;$j++){
      $graha_num_maha = ($start_graha_num+$j)%9;//マハーダシャーの惑星の順番を取得

      //以下、セカンドレベルにアンタルダシャー格納
      $dasha_stamp_plus = 0;//ratio加算用変数準備      
      for($k=0;$k<9;$k++){
        //惑星番号を回転させて、アンタルダシャーkeyに
        $graha_num_anthar = ($graha_num_maha+$k<9)?$graha_num_maha+$k:$graha_num_maha+$k-9;//アンタルダシャーの惑星の順番を回転させ取得
        //惑星ratioをダシャーの120分割タイムスタンプにかけて、アンタルダシャーのtimestampを格納していく
        $anthar_dasha_timestamps[] = round($maha_dasha_timestamps[$j] + $dasha_stamp_plus);//ここでアンタルダシャーを登録！！
        $dasha_ratio_here =  $dasha_ratio[$graha_num_anthar];//今回のratioを導き出す
        // echo $dasha_ratio_here;
        $dasha_stamp_plus = $dasha_stamp_plus + $dasha_ratio_here/120 * $maha_timestamps_D120[$j];//次のstampに備えて、加算しておく。
      }
    }
    // var_dump($anthar_dasha_timestamps);
    return $anthar_dasha_timestamps;
  }

  function get_prati_dasha($anthar_dasha_timestamps,$nak_graha_num){
    $vim_graha = array("Ke","Ve","Su","Mo","Ma","Ra","Ju","Sa","Me");
    $start_graha = $vim_graha[$nak_graha_num];//始まりの惑星
    $dasha_ratio = array("7","20","6","10","7","18","16","19","17");//ダシャー比率定義定義
    $anthar_timestamps_D120  = array();//各アンタルダシャー期間のtimestampを120分割した数値格納配列
    $prati_dasha_timestamps = array();//各9ダシャー期間の中の細分化されたダシャーの開始日をタイムスタンプで表示。最終成果物。
    $start_graha_num = array_search($start_graha, $vim_graha);//始まりの惑星番号を取得

    $count = count($anthar_dasha_timestamps) - 1;//アンタルダシャーの配列数を数えて、一つ引いておく
    //anthar_dasha_timestampsから差分を取って行って120で割り、すべて格納する。
    for($i=0;$i<$count;$i++){
      $anthar_timestamps_D120[] = $anthar_dasha_timestamps[$i+1] - $anthar_dasha_timestamps[$i];
    }
    //配列に3ダシャーを格納していく。
    //セカンドレベルにアンタルダシャー惑星名格納
    for($k=0;$k<$count;$k++){
      //惑星番号を回転させて、アンタルダシャーkeyに
      $diff = floor($k/9); //【重要】マハーダシャーのワンサイクルが終わると、アンタルダシャーのサイクルが一つ飛ばされる対策！！
      $graha_num_anthar = ($nak_graha_num+$k+$diff)%9;//アンタルダシャーの惑星の順番を回転させ取得。マハーダシャーが終わると、一つのアンタルだシャーがスキップされるのを$diffで処理
      $ruler_anthar = $vim_graha[$graha_num_anthar];//アンタルダシャーのルーラー惑星名
      $graha_num_maha = ($nak_graha_num + floor($k/9))%9;//マハーダシャーの算出は、9の惑星をめぐり、10で次のサイクルに入るから、10で割る！！
      $ruler_maha = $vim_graha[$graha_num_maha];
      $dasha_stamp_plus = 0;//ratio加算用変数準備    
      for($l=0;$l<9;$l++){
        $graha_num_prati = ($graha_num_anthar+$l)%9;//アンタルダシャーの惑星の順番を起点に、回転させ取得
        $ruler_prati = $vim_graha[$graha_num_prati];//プラティアンタルダシャーのルーラー惑星名
        //惑星ratioをダシャーの120分割タイムスタンプにかけて、アンタルダシャーのtimestampを格納していく
        $prati_dasha_timestamps[] = array($ruler_maha.$ruler_anthar.$ruler_prati,round($anthar_dasha_timestamps[$k] + $dasha_stamp_plus));//多重配列に、惑星3段階と、timestampを格納！
        $dasha_ratio_here =  $dasha_ratio[$graha_num_prati];//今回のratioを導き出す
        // echo $dasha_ratio_here;
        $dasha_stamp_plus = $dasha_stamp_plus + $dasha_ratio_here/120 * $anthar_timestamps_D120[$k];//次のstampに備えて、加算しておく。
      }//prati
      }//anthar
    return $prati_dasha_timestamps;
  }

//Karakaを算出する
$karaka = array();
for($i=0;$i<7;$i++){
  $karaka[$i][0] = $i+1;
  $karaka[$i][1] = $graha[$i+1];//惑星名
  $karaka[$i][2] = $degInside[$i+1]; 
}
usort($karaka, function ($a, $b) {//角度でsort
    return $b[2] - $a[2];//[2]が角度
});
for($i=0;$i<7;$i++){//カーラカ名を格納
  $karaka[$i][3] = $karaka_name[$i];//[3]にカーラカ名
}
usort($karaka, function ($a, $b) {//惑星番号1Su~7Saで再sort
    return $a[0] - $b[0];//[0]惑星番号
});

//【ナヴァムシャの実装】 
//:::::::::::::::::::::::::::::::::
for($i=0;$i<10;$i++){
  $nava9[$i] = ceil($degInside[$i]/(30/9));//degInsideで9区画に分ける。
  $element4[$i] = $rasiNum[$i] % 4 +1 ;//地水火風の星座属性を把握。
  //4属性で星座番号を割り出す。
  if($element4[$i] == 1){$rasiNumNava[$i] = $nava9[$i]-1;}//火　
  elseif($element4[$i] == 2){$rasiNumNava[$i] = (8 + $nava9[$i] < 12)?8+$nava9[$i]:$nava9[$i]-4 ;}//土
  elseif($element4[$i] == 3){$rasiNumNava[$i] = (5 + $nava9[$i]<12)?5+$nava9[$i]:$nava9[$i]-7;}//風
  else{$rasiNumNava[$i] = 2 + $nava9[$i];}//水
}

  //php to js with json
  function json_enc($arg){  return json_encode($arg, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT);}


?>

<div class="row">
<?php
echo "【".$first_name." ".$family_name."さんのJyotishチャート】<br />";
echo "<div class='col-lg-6 col-md-6'><table border=1 id='basic-chart'>";

//基本情報のtable表示
for($i=0;$i<10;$i++){
  $karakaHere = ($i>0&&$i<8)? $karaka[$i-1][3]:"";//カーラカ表示
  $reverseHere = ($i<8)?$reverse[$i]:"";
  $reverseJp = ($reverse[$i]=="R")?"逆行":"";
  echo "<tr><td class='graha-".$graha[$i]."'>".$graha[$i].$reverseHere."</td><td>".$rasiJP[$rasiNum[$i]].$degInsideMin[$i]."</td><td>".$houseNum[$i]."室</td><td>".$karakaHere."</td><td>速度".$speed2min[$i].$reverseJp ."</td><td>".$nakName[$i]." (<span class='graha-".$nakGrahaName[$i]."'>".$nakGrahaName[$i]."</span>)</td></tr>";
}
  echo "</table></div><div class='col-lg-6 col-md-6'>";
  
  echo "</div>";

?>

</div><!-- end row -->
</div>

<!-- 南インド式ラーシチャート開始 -->
<div id="charts" class="container">
<div class="row">
<div class="chart-table col-md-6 col-lg-6 col-sm-12 col-xs-12">
<table id="rasi-chart">
  <?php
    $rasi_td_array = array(12,1,2,3,11,0,0,4,10,0,0,5,9,8,7,6);//南インド式ラーシ番号とtd
    for ($i = 0; $i < 16; $i++) {
      if($i==0){echo"<tr>";}
        elseif($i%4==0){echo "</tr><tr>";}
      $str = sprintf("<td colspan=2 rowspan=2 style='text-align:center;'>D1 Rasi chart<br><br>%s %sさん<br><br>%d年%d月%d日 %d時%d分<br>N%01.2f E%01.2f<br>%s</td>",$family_name,$first_name,$year,$month,$day,$hour,$min,$lat,$lng,$birth_place);
      if($rasi_td_array[$i]==0){if($i==5){echo $str;}}//中央を空白にする
      else{$tmpName = $rasiJP[$rasi_td_array[$i]-1];//星座名表示
        $tmpNum = $rasi_td_array[$i] - $rasiNum[0];//ハウスナンバー表示
        $tmpNum = ($tmpNum<1)?$tmpNum+12:$tmpNum;//ハウスナンバーのマイナスを解消
        echo "<td id='rasi_$rasi_td_array[$i]' class='chart-td rasi-td'><span class='rasi-name'>$tmpName<span><span class='house-num'>$tmpNum<span></td>";
      }
      if($i==15){echo "</tr>";}
    }
  ?>
  <script>
    //jsonでphpからjsに値を渡す
    var rasiNum = <?php echo json_enc($rasiNum); ?>;
    var graha =  <?php echo json_enc($graha); ?>;
    var degInside =  <?php echo json_enc($degInside); ?>;
    var degInsideMin =  <?php echo json_enc($degInsideMin); ?>;
    var karaka =  <?php echo json_enc($karaka); ?>;
    var reverse = <?php echo json_enc($reverse); ?>;

    //jQueryで各惑星名と度数を該当ハウスに表示 
    $(function(){
      for(var i=0;i<10;i++){
        var tmp = "#rasi_"+(rasiNum[i]+1)+" .rasi-name";
        var karakaHere = (i>0&&i<8)? karaka[i-1][3]:"";//カーラカ表示
        var reverseHere = (i<8)?reverse[i]:"";
        $(tmp).after("<br /><span id='graha-"+graha[i]+"'><span class='graha-span graha-"+graha[i]+"'>"+graha[i]+reverseHere+"</span> "+degInsideMin[i]+" "+karakaHere+"</span>");      
      }
      $("#rasi_"+(rasiNum[0]+1)).addClass("asc-td");//アセンダントに背景色を付与
    })
  </script>
</table>
</div>
<!-- 南インド式ラーシチャート終了 -->


<!-- 南インド式ナヴァムシャチャート開始 -->
<div class="chart-table col-md-6 col-lg-6 col-sm-12 col-xs-12">
<table id="nava-chart">
  <?php
    $rasi_td_array = array(12,1,2,3,11,0,0,4,10,0,0,5,9,8,7,6);
    for ($i = 0; $i < 16; $i++) {
      if($i==0){echo"<tr>";}
        elseif($i%4==0){echo "</tr><tr>";}
      $str = sprintf("<td colspan=2 rowspan=2 style='text-align:center;'>D9 Navamsha chart<br><br>%s %sさん<br><br>%d年%d月%d日 %d時%d分<br>N%01.2f E%01.2f<br>%s</td>",$family_name,$first_name,$year,$month,$day,$hour,$min,$lat,$lng,$birth_place);
      if($rasi_td_array[$i]==0){if($i==5){echo $str;}}//中央を空白にする
        else{$tmpName = $rasiJP[$rasi_td_array[$i]-1];//星座名表示
          $tmpNum = $rasi_td_array[$i] - $rasiNumNava[0];//ハウスナンバー表示
          $tmpNum = ($tmpNum<1)?$tmpNum+12:$tmpNum;//ハウスナンバーのマイナスを解消
        echo "<td id='nava_$rasi_td_array[$i]' class='chart-td nava-td'><span class='rasi-name'>$tmpName<span><span class='house-num'>$tmpNum<span></td>";}
      if($i==15){echo "</tr>";}
    }
  ?>
  <script>
    var rasiNumNava =  <?php echo json_enc($rasiNumNava); ?>; 
    //jQueryで各惑星名と度数を該当ハウスに表示 
    $(function(){
      for(var i=0;i<10;i++){
        var tmp = "#nava_"+(rasiNumNava[i]+1)+" .rasi-name";//惑星の差込位置を取得
        var karakaHere = (i>0&&i<8)? karaka[i-1][3]:"";//カーラカ表示
        var reverseHere = (i<8)?reverse[i]:"";
        $(tmp).after("<br /><span id='graha-"+graha[i]+"'><span class='graha-span graha-"+graha[i]+"'>"+graha[i]+reverseHere+"</span> "+karakaHere+"</span>"); 
      }
      $("#nava_"+(rasiNumNava[0]+1)).addClass("asc-td");//アセンダントに背景色を付与
    })
  </script>
</table>
<!-- 南インド式ナヴァムシャチャート終了 -->
</div><!-- col -->
</div><!-- row -->

<!-- ヴィムショタリダシャー表示 -->
<div class='col-lg-6 col-md-6 col-sm-12'>
<h3>ヴィムショタリダシャー</h3>
<?php
  $maha_dasha = get_maha_dasha($mo[0],$nakNum[2], $nakGrahaNum[2],$nakName[2], $utcBirthTimestamp);
  $anthar_dasha = get_anthar_dasha($maha_dasha,$nakGrahaNum[2]);
  $prati_dasha = get_prati_dasha($anthar_dasha,$nakGrahaNum[2]);
  $countPrati = count($prati_dasha);
  $maha_count = 0;//次のマハーダシャー情報をゲットするため

  function get_age($stamp,$utc){
    return floor(($stamp - $utc)/(24*3600*365))."歳"; //年齢取得
  }

  for($i=0;$i<$countPrati;$i++){
    $maha = substr($prati_dasha[$i][0],0,2);
    $anthar = substr($prati_dasha[$i][0], 2,2);
    $prati = substr($prati_dasha[$i][0], 4,2); 
    $val = $prati_dasha[$i][1];

    $age = get_age($val,$utcBirthTimestamp); //年齢取得
    
    if($i%81 ==0){//マハーダシャー情報をタイトルにする
      $age_next = floor(($maha_dasha[$maha_count+1] - $utcBirthTimestamp)/(24*3600*365))."歳"; //ダシャーの終わりの年連取得
      $date = date('Y年m月',$maha_dasha[$maha_count]);
      $date_next = date('Y年m月',$maha_dasha[$maha_count+1]);//とりあえず110歳以降は見ない

      if($maha_count != 0){
        echo "</div><!--end maha-".($maha_count-1)." -->";
      }
      echo "<span class='maha-title'>■<span class='graha-".$maha."'>".$maha."</span>期 ".$date."(".$age.") - ".$date_next."(".$age_next.")</span><br />";//マハーダシャータイトル表示
      echo "<div class='maha-gross' id='maha-".$maha_count."'>";
      $maha_count++;
    }
    echo "<strong><span class='graha-".$maha."'>".$maha."</span>-<span class='graha-".$anthar."'>".$anthar."</span>-<span class='graha-".$prati."'>".$prati."</span></strong>:".get_date($val)."(".$age.")<br />";
    // echo "<strong><span class='graha-".$key."'>".$key."</span></strong>:".get_date($val)."<br />";
  }
  echo "</div><!--end maha-".($maha_count-1)." -->";//最後の締め
?>
</div><!-- col-lg-6 -->
</div><!-- container -->
</div><!-- #charts end -->
<br />

</body>

<script src="https://www.gstatic.com/firebasejs/4.3.1/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAN8amT7T4wMvZH37-TV_fVqGPxI6YD2t4",
    authDomain: "jyotish-one.firebaseapp.com",
    databaseURL: "https://jyotish-one.firebaseio.com",
    projectId: "jyotish-one",
    storageBucket: "jyotish-one.appspot.com",
    messagingSenderId: "99716757920"
  };
  firebase.initializeApp(config);
</script>

</html>
