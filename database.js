/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Initializes JyotishOne.
function JyotishOne() {
  this.checkSetup();

  // Shortcuts to DOM Elements.
  this.messageList = document.getElementById('messages');
  this.messageList2 = document.getElementById('messages2');
  this.messageList3 = document.getElementById('messages3');
  this.messageForm = document.getElementById('form');
  this.messageInput = document.getElementById('message');
  this.submitButton = document.getElementById('submit');
  // this.submitImageButton = document.getElementById('submitImage');
  // this.imageForm = document.getElementById('image-form');
  // this.mediaCapture = document.getElementById('mediaCapture');
  this.userPic = document.getElementById('user-pic');
  this.userName = document.getElementById('user-name');
  this.signInButton = document.getElementById('sign-in');
  this.signOutButton = document.getElementById('sign-out');
  this.signInSnackbar = document.getElementById('must-signin-snackbar');

  // Saves message on form submit.
  this.messageForm.addEventListener('submit', this.saveMessage.bind(this));
  this.messageForm2.addEventListener('submit', this.saveMessage.bind(this));
  this.messageForm3.addEventListener('submit', this.saveMessage.bind(this));
  this.signOutButton.addEventListener('click', this.signOut.bind(this));
  this.signInButton.addEventListener('click', this.signIn.bind(this));

  this.initFirebase();
}
// A loading image URL.
JyotishOne.LOADING_IMAGE_URL = 'https://www.google.com/images/spin-32.gif';

// firebaseの各機能へのショートカットと、認証の初期化
JyotishOne.prototype.initFirebase = function() {
  // Firebase SDK へのショートカット
  this.auth = firebase.auth();
  this.database = firebase.database();
  this.storage = firebase.storage();
  // Initiates Firebase auth and listen to auth state changes.
  this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));

  // this.usersRef = this.database.ref("users");
  // if (this.checkSignedInWithMessage()) {
  //   this.usersRef.push({//pushは強制的に新規投稿を作成
  //     name: this.auth.currentUser.displayName,
  //     uid: this.auth.currentUser.uid
  //   });
  // };
};

// Saves a new message on the Firebase DB.
JyotishOne.prototype.saveMessage = function(e) {
  e.preventDefault();

  var nowHere = new Date();
  var year = formatYear(nowHere);
  var month = formatMonth(nowHere);
  var day = formatDay(nowHere);
  var hour = formatHour(nowHere);
  var min = formatMin(nowHere);
  
  var txt ="";
  var whichInput ="";//どのフィールドに追加されたか確認して、txtに登録
  if(this.messageInput.value){
    txt = this.messageInput.value;
    whichInput = 1;
  }else if(this.messageInput2.value){
    txt = this.messageInput2.value;
    whichInput = 2;
  }else if(this.messageInput3.value){
    txt = this.messageInput3.value;
    whichInput = 3;
  }

  var tagNum = txt.indexOf("#");//ハッシュタグゲット
  var tag="";
  if(tagNum!=-1){tag = txt.substring(tagNum+1);}//タグの中身を取得

  var j_typeHere = "";//記事タイプ取得準備
  var stars = 0;
  if (txt && this.checkSignedInWithMessage()) {
    if(whichInput==1){
      j_typeHere = $('input:radio[name="j-type"]:checked').val();//radioからの値を取得
      stars = $('input:radio[name="stars"]:checked').val();//星の数取得
    }else if(whichInput==2){
      j_typeHere = $('input:radio[name="j-type2"]:checked').val();
      stars = $('input:radio[name="stars2"]:checked').val();//星の数取得
    }else if(whichInput==3){
      j_typeHere = $('input:radio[name="j-type3"]:checked').val();
      stars = $('input:radio[name="stars3"]:checked').val();//星の数取得
    }

    // firebaseに新しい記事を登録
    this.messagesRef.push({//pushは強制的に新規投稿を作成
      year:year,
      month:month,
      day:day,
      hour:hour,
      min:min,
      text: txt,
      j_type:j_typeHere,
      tag:tag,
      timestamp:year+month+day+hour+min+"",
      stars:stars
    }).then(function() {
      // 該当テキストフィールドをクリア
      if(whichInput==1){
        JyotishOne.resetMaterialTextfield(this.messageInput);
      }else if(whichInput==2){
        JyotishOne.resetMaterialTextfield(this.messageInput2);
      }else if(whichInput==3){
        JyotishOne.resetMaterialTextfield(this.messageInput3);
      }
      // this.toggleButton();
    }.bind(this)).catch(function(error) {
      console.error('Error writing new message to Firebase Database', error);
    });
  }
};


// Loads chat messages history and listens for upcoming ones.
JyotishOne.prototype.loadMessages = function() {

  var currentUser = this.auth.currentUser;
  var uid = currentUser.uid;
  console.log(uid);
  //var name = currentUser.displayName;

  // データベースへの参照。google アカウントでその人にしか記事が見えなくする措置。uidは、アプリごとに変わる。端末では変わらない。
  this.messagesRef = this.database.ref(uid);
  // Make sure we remove all previous listeners.
  this.messagesRef.off();

  // Loads the last 20 messages and listen for new ones.
  var setMessage = function(data) {
    var val = data.val();

    var year = val.year;
    var month = val.month;
    var day = val.day;
    var hour = val.hour;
    var min = val.min;
    var j_type = (val.j_type)? val.j_type:"";
    var tag = (val.tag)? val.tag:"";
    var stars = val.stars;
  
    this.displayMessage(data.key, year,month,day,hour,min , val.text, j_type, tag, stars, currentUser.photoURL, val.imageUrl);
  }.bind(this);
  this.messagesRef.limitToLast(30).on('child_added', setMessage);
  this.messagesRef.limitToLast(30).on('child_changed', setMessage);
};

// Template for messages.
JyotishOne.MESSAGE_TEMPLATE =
    '<div class="message-container">' +
      '<div class="spacing"><div class="dateArea"></div><div class="timeArea"></div><div class="starsArea"></div></div>' +
      '<div class="message"><span class="strong_text"></span><span class="normal_text"></span></div>' +
      '<div class="date"><span class="strong_date"></span><span class="normal_date"></span></div><span class="j_type"></span>' +
    '</div>';

// 記事の表示
JyotishOne.prototype.displayMessage = function(key, year,month,day,hour,min, text, j_type, tag, stars, picUrl, imageUri) {
  var div = document.getElementById(key);
  // If an element for that message does not exists yet we create it.

  var focusInput ="";//表示後にfocusを当てるところを登録
  var focusList ="";//表示後にスクロールさせるため
  if (!div) {
    var container = document.createElement('div');
    container.innerHTML = JyotishOne.MESSAGE_TEMPLATE;
    div = container.firstChild;
    div.setAttribute('id', key);
    if(j_type == "todo"||j_type=="done"){
      this.messageList2.appendChild(div);
      focusInput = this.messageInput2;
      focusList = this.messageList2;
    }else if(j_type == "inside"||j_type=="insight"){
      this.messageList3.appendChild(div);
      focusInput = this.messageInput3;
      focusList = this.messageList3;
    }else{    
      this.messageList.appendChild(div);
      focusInput = this.messageInput;
      focusList = this.messageList;
    }
  }

  //はじめのパラグラフを太字にするように設定して、テキスト表示
  var period = "";
  var paragraph_strong = "";
  var paragraph_normal = "";
  var period1,period2;//。と！の判別をするため

  if(text.indexOf("!")!=-1||text.indexOf("！")!=-1){
    period1 = (text.indexOf("!")!=-1)?text.indexOf("!"):text.indexOf("！");
  }
  if(text.indexOf("。")!=-1||text.indexOf(".")!=-1){
    period2 = (text.indexOf("。")!=-1)?text.indexOf("。"):text.indexOf(".");
  }
  if(period1||period2){//。か！がある場合
    if(period1&&period2){//両方の場合
      period = (period1 < period2)?period1:period2;
    }else if(period1){
      period = period1;
    }else if(period2){
      period = period2;
    }
    paragraph_strong = text.substr(0,period+1);
    paragraph_normal = text.substring(period+1);
  }else{//。も！もない場合
    paragraph_strong = text;
  }

  var messageElement = div.querySelector('.message');
  var strongElement = div.querySelector('.strong_text');
  var normalElement = div.querySelector('.normal_text');

  //日付関連
  var monthHere = (month.substr(0,1)==0)?month.substr(1,1):month;
  var dayHere = (day.substr(0,1)==0)?day.substr(1,1):day;
  var timeHere = hour+":"+min;
  var dateStrong = monthHere+"月"+dayHere+"日";
  var dateNormal = timeHere+"  ----"+year+"年";
  // div.querySelector('.strong_date').textContent = dateStrong;
  div.querySelector('.normal_date').textContent = dateStrong+" "+dateNormal+"　"+ j_type+" "+tag;
  div.querySelector('.dateArea').textContent = dayHere;
  div.querySelector('.timeArea').textContent = timeHere;

  var starsHere = "";
  if(stars){
    for(var i=0;i<stars;i++){
      starsHere = starsHere+"★";
    }
  }
  //星関連
  div.querySelector('.starsArea').textContent = starsHere;  

  if (text) { //  記事が文章の場合
    // messageElement.textContent = str;
    strongElement.textContent = paragraph_strong;
    normalElement.textContent = paragraph_normal;
    // Replace all line breaks by <br>.
    normalElement.innerHTML = normalElement.innerHTML.replace(/\n/g, '<br>');

  } else if (imageUri) { // 記事が画像の場合
    var image = document.createElement('img');
    image.addEventListener('load', function() {
      this.messageList.scrollTop = this.messageList.scrollHeight;
    }.bind(this));
    this.setImageUrl(imageUri, image);
    messageElement.innerHTML = '';
    messageElement.appendChild(image);
  }
  // Show the card fading-in.
  setTimeout(function() {div.classList.add('visible')}, 1);
  focusList.scrollTop = focusList.scrollHeight;
  focusInput.focus();
};

// サインイン
JyotishOne.prototype.signIn = function() {
  var provider = new firebase.auth.GoogleAuthProvider();
  this.auth.signInWithPopup(provider);
};
// サインアウト
JyotishOne.prototype.signOut = function() {
  this.auth.signOut();
};

// Triggers when the auth state change for instance when the user signs-in or signs-out.
JyotishOne.prototype.onAuthStateChanged = function(user) {
  if (user) { // User is signed in!
    // Get profile pic and user's name from the Firebase user object.
    var profilePicUrl = user.photoURL; 
    var userName = user.displayName; 
    // var userName = user.displayName;   

    // Set the user's profile pic and name.
    this.userPic.style.backgroundImage = 'url(' + profilePicUrl + ')';
    this.userName.textContent = userName;

    // Show user's profile and sign-out button.
    this.userName.removeAttribute('hidden');
    this.userPic.removeAttribute('hidden');
    this.signOutButton.removeAttribute('hidden');

    // Hide sign-in button.
    this.signInButton.setAttribute('hidden', 'true');

    // We load currently existing messages.
    this.loadMessages();

  } else { // User is signed out!
    // Hide user's profile and sign-out button.
    this.userName.setAttribute('hidden', 'true');
    this.userPic.setAttribute('hidden', 'true');
    this.signOutButton.setAttribute('hidden', 'true');

    // Show sign-in button.
    this.signInButton.removeAttribute('hidden');
  }
};

// Returns true if user is signed-in. Otherwise false and displays a message.
JyotishOne.prototype.checkSignedInWithMessage = function() {
  /* TODO(DEVELOPER): Check if user is signed-in Firebase. */
  if (this.auth.currentUser) {
    return true;
  }

  // Display a message to the user using a Toast.
  var data = {
    message: 'You must sign-in first',
    timeout: 2000
  };
  this.signInSnackbar.MaterialSnackbar.showSnackbar(data);
  return false;
};


// Resets the given MaterialTextField.
JyotishOne.resetMaterialTextfield = function(element) {
  element.value = '';
  element.parentNode.MaterialTextfield.boundUpdateClassesHandler();
};


// Checks that the Firebase SDK has been correctly setup and configured.
JyotishOne.prototype.checkSetup = function() {
  if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
    window.alert('You have not configured and imported the Firebase SDK. ' +
        'Make sure you go through the codelab setup instructions and make ' +
        'sure you are running the codelab using `firebase serve`');
  }
};

window.onload = function() {
  window.jyotishOne = new JyotishOne();
};

/**
 * 日付をフォーマットする
 * @param  {Date}   date     日付
 * @return {String}          フォーマット済み日付
 */
var formatYear = function (date) {
  var format = 'YYYY';
  format = format.replace(/YYYY/g, date.getFullYear());
  return format;
};
var formatMonth = function (date) {
  var format = 'MM';
  format = format.replace(/MM/g, ('0' + (date.getMonth() + 1)).slice(-2));
  return format;
};
var formatDay = function (date) {
  var format = 'DD';
  format = format.replace(/DD/g, ('0' + date.getDate()).slice(-2));
  return format;
};
var formatHour = function (date) {
  var format = 'hh';
  format = format.replace(/hh/g, ('0' + date.getHours()).slice(-2));
  return format;
};
var formatMin = function (date) {
  var format = 'mm';
  format = format.replace(/mm/g, ('0' + date.getMinutes()).slice(-2));
  return format;
};

